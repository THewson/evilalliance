﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield_Script : MonoBehaviour {

    public Game_Controller gameController;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            gameController.shieldPointsCurrent -= 10;
        }
        if(other.gameObject.tag == "BadBullet")
        {
            other.gameObject.SetActive(false);
        }
    }
}
