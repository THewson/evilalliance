﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaveData", menuName = "WaveData")]
public class Wave_Template : ScriptableObject {

    public List<int> enemySpawnAmounts;

    [Tooltip("This number is how often an enemy spawns")]
    public float spawnRate;

    public string waveName;

    public List<int> enemySpeed;

    [Tooltip("Shooting rate of shooting enemy")]
    public int shootingEnemyRate;
}
