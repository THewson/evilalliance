﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnBadBulletsOnContact : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "BadBullet")
        {
            other.gameObject.SetActive(false);
        }
    }
}
