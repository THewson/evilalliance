﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Character_Controller : MonoBehaviour {

    public GameObject m_Shield;
    public GameObject m_Gun;
    public Camera m_Camera;

    public Quaternion m_ShieldRotation;
    public Vector3 m_ShieldPosition;

    public Transform m_BulletSpawnPoint;
    public Transform m_LineEnd;

    Vector3 rotationalAxis = new Vector3(0, 1, 0);
    public float ShieldRotationSpeed = 5.0f;

    private Bullet_Manager m_BulletManager;
    private Vector3 mousePos;

    public Game_Controller m_gameController;

    // Use this for initialization
    void Start () {
        m_ShieldRotation = m_Shield.transform.rotation;
        m_ShieldPosition = m_Shield.transform.position;

        GameObject m_GameController = GameObject.FindGameObjectWithTag("GameController");

        m_BulletManager = m_GameController.GetComponent<Bullet_Manager>();
	}

    private void LateUpdate()
    {
        if (Time.timeScale == 1)
        {
            if (Input.GetKey(KeyCode.A))
            {
                m_Shield.transform.RotateAround(this.gameObject.transform.position, rotationalAxis, -ShieldRotationSpeed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.D))
            {
                m_Shield.transform.RotateAround(this.gameObject.transform.position, rotationalAxis, ShieldRotationSpeed * Time.deltaTime);
            }

            if (Input.GetMouseButtonDown(0))
            {
                m_BulletManager.SpawnBullet(m_BulletSpawnPoint.position, true, this.gameObject.transform.rotation);
            }

        Vector3 vec1 = (Camera.main.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 50.0f));
        this.gameObject.transform.LookAt(new Vector3(vec1.x, 0));

        }

        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene("MainGame_EvilAlliance");
        }

        // 0 the x & z to remove floating point errors.
        Vector3 angles = gameObject.transform.eulerAngles;
        angles.x = angles.z = 0;
        gameObject.transform.eulerAngles = angles;
    }
}
