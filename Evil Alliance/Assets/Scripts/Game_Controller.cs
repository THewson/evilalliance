﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Controller : MonoBehaviour
{

    public Bullet_Manager bulletManager;

    public GameObject pauseMenu;
    public GameObject gameOverMenu;
    public GameObject controlsMenu;

    public int healthPointsMax = 100;
    public int shieldPointsMax = 100;

    public int healthPointsCurrent;
    public int shieldPointsCurrent;

    public int agb;
    public int gbp;

    public bool m_EndGame;

    public float totalrechargeTime = 3.0f;
    public float rechargeTime = 3.0f;
    private void Start()
    {
        healthPointsCurrent = healthPointsMax;
        shieldPointsCurrent = shieldPointsMax;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);

            if (pauseMenu.activeSelf) { Time.timeScale = 0; }
            if (!pauseMenu.activeSelf) { Time.timeScale = 1; }
        }

        //   m_Health.text = ": " + healthPointsCurrent+ " / " + healthPointsMax;
        //   m_Shield.text = ": " + shieldPointsCurrent+ " / " + shieldPointsMax;
        //   m_Ammo.text = ": ";

        if (shieldPointsCurrent <= 0)
        {
            rechargeTime -= Time.deltaTime;

            if (rechargeTime <= 0)
            {
                shieldPointsCurrent = shieldPointsMax;
                rechargeTime = totalrechargeTime;
            }
        }
        if (controlsMenu.gameObject.activeSelf)
        {
            Cursor.visible = true;
            Time.timeScale = 0;
        }

        if (!controlsMenu.gameObject.activeSelf)
        {
            Cursor.visible = false;
            Time.timeScale = 1;
        }
    }
}

