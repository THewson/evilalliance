﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YourBadLol : MonoBehaviour {

    //public Text text;
    public Canvas gameOverCanvas;
    //public Text healthPercentage;
    public GameObject missedMemeAudioOne;
    public GameObject missedMemeAudioTwo;
    public GameObject missedMemeAudioThree;
    public GameObject missedMemeAudioFour;
    public GameObject missedMemeAudioFive;
    public GameObject audioPlayer;

    public string string1 = "";
    public int points = 5;

    public Text healthDisplay;

    private void Start()
    {
     //   text.text = string1 + points;
        gameOverCanvas.enabled = false;
        Time.timeScale = 1;
        healthDisplay.text = "5";

       // healthPercentage.text = ": 100%";
    }

    private void OnTriggerEnter(Collider other)
    {                
        if(other.tag == "Enemy" || other.tag == "BadBullet")
        {
            other.gameObject.SetActive(false);
            Debug.Log ("Enemy hit you fool");
            points--;

            if (points == 0)
            {
                Debug.Log("No points son!");
                gameOverCanvas.enabled = true;
                Time.timeScale = 0;
                Cursor.visible = true;
            }
        }
        if(points == 4)
        {
            missedMemeAudioOne.SetActive(true);
            healthDisplay.text = points.ToString();
        }
        else if(points == 3)
        {
            missedMemeAudioTwo.SetActive(true);
           
            healthDisplay.text = points.ToString();
        }
        else if (points == 2)
        {
            missedMemeAudioThree.SetActive(true);
            healthDisplay.text = points.ToString();
        }
        else if (points == 1)
        {
            missedMemeAudioFour.SetActive(true);
            healthDisplay.text = points.ToString();
        }
        else if (points == 0)
        {
            missedMemeAudioFive.SetActive(true);
            audioPlayer.SetActive(false);
            healthDisplay.text = points.ToString();
        }
    }
}
