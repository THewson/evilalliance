﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting_Enemy_Script : MonoBehaviour {

    public float m_SecondsPerShot;
    private float m_spsINTERNAL;
    public bool m_JustSpawned;

    public Wave_Manager waveManager;
    public Bullet_Manager bulletManager;

    public Transform m_bulletSpawnPoint;

    public GameObject turret;

    public bool m_ReadyToShoot;

    private void Start()
    {
        bulletManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game_Controller>().bulletManager;
    }

    // Update is called once per frame
    void LateUpdate () {
        if (m_JustSpawned)
        {
            m_spsINTERNAL = m_SecondsPerShot;
            m_ReadyToShoot = false;

            GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(waveManager.m_ShootingTarget.transform.position);

            m_JustSpawned = false;
        }

        turret.transform.LookAt(waveManager.m_Target.transform.position);
        m_bulletSpawnPoint.transform.LookAt(waveManager.m_Target.transform.position);
        m_spsINTERNAL -= Time.deltaTime;

        if(m_spsINTERNAL <= 0)
        {
            bulletManager.SpawnBullet(m_bulletSpawnPoint.position, false, this.transform.rotation);

            Debug.Log("Bewm");
            m_spsINTERNAL = m_SecondsPerShot;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet") {waveManager.AddScore(); Destroy(this.gameObject); }
        if (other.gameObject.tag == "Player") { Destroy(this.gameObject); }
        if (other.gameObject.tag == "Shield") { waveManager.AddScore(); Destroy(this.gameObject); }
        if (other.gameObject.tag == "ShootyBoyBarrier") { GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(this.transform.position); }
    }
}