﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wave_Manager : MonoBehaviour {
    float m_TimeUntilNextEnemySpawn;
    public float m_SecondsUntilNextWave;

    int m_CurrentWaveIndex;
    public int m_totalEnemiesLeft;
    
    public bool m_NewWave;

    Wave_Template m_CurrentWave;
    public Game_Controller m_GameController;
    public GameObject m_Target;
    public GameObject m_ShootingTarget;

    public List<Wave_Template> m_Waves;
    public List<Transform> m_SpawnZones;
    public List<GameObject> m_EnemyTemplates;
    List<int> m_EnemySpawnAmountLeft;

    public Text score;
    private int scoreint = 0;

    Text m_WaveTextField;

    public void AddScore()
    {
        scoreint += 10;
    }

	// Use this for initialization
	void Start () {
        m_totalEnemiesLeft = 0;
        m_CurrentWave = m_Waves[0];
        m_CurrentWaveIndex = 0;
        m_EnemySpawnAmountLeft = new List<int>();
        m_EnemySpawnAmountLeft = m_CurrentWave.enemySpawnAmounts;

        for(int  i = 0; i != m_EnemySpawnAmountLeft.Count; i++)
        {
            m_totalEnemiesLeft += m_EnemySpawnAmountLeft[i];
        }

        score.text = "0000";
	}

    void NewWaveUpdate()
    {
        m_EnemySpawnAmountLeft = m_CurrentWave.enemySpawnAmounts;
        m_TimeUntilNextEnemySpawn = m_CurrentWave.spawnRate;
        m_totalEnemiesLeft = 0;

        for (int i = 0; i != m_EnemySpawnAmountLeft.Count; i++)
        {
            m_totalEnemiesLeft += m_EnemySpawnAmountLeft[i];
        }
    }

    void SpawnEnemy()
    {
        if (m_totalEnemiesLeft > 0)
        {
            int num = Random.Range(0, m_EnemySpawnAmountLeft.Count);
            if (m_EnemySpawnAmountLeft[num] > 0)
            {
                int i = Random.Range(0, m_SpawnZones.Count);

                m_totalEnemiesLeft--;
                GameObject go = Instantiate<GameObject>(m_EnemyTemplates[num], m_SpawnZones[i]);
                go.GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(m_SpawnZones[i].transform.position);
                if(num == 0)
                {
                    go.GetComponent<Kamakazi_Script>().waveManager = this;
                }
                if(num == 1)
                {
                    go.GetComponent<Shooting_Enemy_Script>().waveManager = this;
                    go.GetComponent<Shooting_Enemy_Script>().m_JustSpawned = true;
                    go.GetComponent<Shooting_Enemy_Script>().m_SecondsPerShot = m_CurrentWave.shootingEnemyRate;
                }
            }
            else
            {
                SpawnEnemy();
            }
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (m_NewWave)
        {
            if(m_CurrentWaveIndex == m_Waves.Count)
            {
                m_GameController.m_EndGame = true;
            }
            else
            {
                m_CurrentWaveIndex++;
                m_CurrentWave = m_Waves[m_CurrentWaveIndex];
            }
            m_NewWave = false;
        }

        m_TimeUntilNextEnemySpawn -= Time.deltaTime;
        if(m_TimeUntilNextEnemySpawn <= 0)
        {
            m_TimeUntilNextEnemySpawn = m_CurrentWave.spawnRate;
            SpawnEnemy();
        }

        score.text = scoreint.ToString();
	}
}
