﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnBulletsOnCollission : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Bullet")
        {
            other.gameObject.SetActive(false);
        }
    }
}
