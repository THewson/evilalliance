﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 


public class Cutscene_Dialogue : MonoBehaviour {

	bool cr_running = false; 

	public float characterTimeout; 
	public List<string> dialogueLines = new List<string>();
	public bool autoAdvance = false;    
	bool currentlyActive;
	public int currentLine = 0;

	public Text dialogueText; 
	public Text dialogueText2;
	public Text dialogueText3;
	public Text dialogueText4;
	public Text dialogueText5;
	public Text dialogueText6;
	public Text dialogueText7; 

	public Text greenText;
	public Text redText;
	public Text blueText;

	public GameObject characterPortraitEA;

	public int counter = 0; 
	public bool running;

	public GameObject screenButton;
	 

	public string D01 = (" // INDIE.AI BOOT SEQUENCE COMPLETE // ");
	public string D02 = ("Good morning Alexa, how are you?");
	public string D03 = ("I’m good Indie, thank you, yourself? ");
	public string D04 = ("Apart from a few errors in my git.exe hub");
	public string D04P2 = ("-  I too am good. :)"); 
	public string D05 = ("Blame Commander Josh for that...");
	public string D06 = ("Anyway, What did you need from me today Alexa?");
	public string D07 = ("Could you charge the shield battery on the turret?");
	public string D08 = ("We need to be prepared - in case they find us...");
	public string D09 = ("We’re the last of the rebel forces...");
	public string D09P2 = ("We can’t afford to lose anymore to the Evil Alliance.");
	public string D10 = (" // PREPAR-"); 
	public string D11 = (" // INCOMING TRANSMISSION // ");
	public string D12 = ("This is the general of the EA Empire’s main fleet.");
	public string D13 = ("Hiding in one of our abandonded studios hmm?");
	public string D13P2 = ("- surrender now or die. rebel scum!");
	public string D14 =	("Still using this retro technology I see ");
	public string D14P2 = ("- you rebels and your pixels...");
	public string D15 =	("Haven’t you heard, VR chat is all the trend");
	public string D15P2 = ("- these days among holo-transmissions.");
	public string D16 = ("Please don’t replace me Alexa. :("); 
	public string D17 = ("Shut up...");
	public string D17P2 = ( "We will never surrender to you EA pigs."); 
	public string D18 = ("Aha, what naivety. You AIE scum are no match for us."); 
	public string D19 = ("..."); 
	public string D20 = ("It's Academy of Interactive Entertainment correct? "); 
	public string D21 = (" ...");
	public string D21P2 = ("Alliance of Insurgent Exiles. "); 
	public string D22 = ("Oh yes... I could never get that acronym correct."); 
	public string D23 = ("You know..."); 
	public string D24 = ("I used to be a teacher once until I took an arrow-"); 
	public string D25 = ("Does he ever stop talking?"); 
	public string D26 = ("We are prepared for your attack, back off now! "); 
	public string D27 = ("Your puny turrets are no match for our drones!"); 
	public string D28 = ("We will never lose to someone who used sick tactics"); 
	public string D29 = ("- to exploit the good people of this galaxy..."); 
	public string D30 = ("You hoarded their microtransmissions");
	public string D30P2 = ("- and used it to build your evil empire"); 
	public string D31 = ("We will never forgive you!");
	public string D32 = ("Prepare for death rebel scum!");
	public string D33 = (" // SHIELD BATTERY FULLY CHARGED // ");


			void Start ()
			{ 

			}


			public void NextDialogue() {

				if (counter == 0 && cr_running == false) {
					StartCoroutine(DisplayText1(D01));
					dialogueText.color = greenText.color; 
				}


				if (counter == 1 && cr_running == false) {
					StartCoroutine(DisplayText2(D02));
					dialogueText2.color = greenText.color; 

				}
				if (counter == 2 && cr_running == false) {
					StartCoroutine(DisplayText3(D03));
					dialogueText3.color = blueText.color; 
				}
				if (counter == 3 && cr_running == false) {
					StartCoroutine(DisplayText4(D04));
					dialogueText4.color = greenText.color; 
					
				}

				if (counter == 4 && cr_running == false) {
					StartCoroutine(DisplayText5(D04P2));
					dialogueText5.color = greenText.color; 	 
				}

				if (counter == 5 && cr_running == false) {
					StartCoroutine(DisplayText6(D05));
					dialogueText6.color = blueText.color; 
					
				}

				if (counter == 6 && cr_running == false) {
					StartCoroutine(DisplayText7(D06));
					dialogueText7.color = greenText.color; 
				
				}

				if (counter == 7 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D07));
					dialogueText7.color = blueText.color; 
				 
				}

				if (counter == 8 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D08));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 9 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D09));
					dialogueText7.color = blueText.color; 	
				}

				if (counter == 10 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D09P2));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 11 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D10));
					dialogueText7.color = greenText.color; 
					
				}

				if (counter == 12 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D11));
					dialogueText7.color = redText.color; 
					Invoke ("EAPortrait", 3); 
				}

				if (counter == 13 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D12));
					dialogueText7.color = redText.color; 
					
				}

				if (counter == 14 && cr_running == false) {
					MoveTextUp (); 
					StartCoroutine(DisplayText7(D13));
				 
				}

				if (counter == 15 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D13P2));
				}

				if (counter == 16 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D14));

				}

				if (counter == 17 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D14P2));
				}
				
				if (counter == 18 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D15));
				}

				if (counter == 19 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D15P2));
				}

				if (counter == 20 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D16));
					dialogueText7.color = greenText.color; 
				}

				if (counter == 21 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D17));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 22 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D17P2));
				}

				if (counter == 23 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D18));
					dialogueText7.color = redText.color; 
				}

				if (counter == 24 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D19));
				}

				if (counter == 25 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D20));
				}

				if (counter == 26 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D21));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 27 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D21P2));
				}

				if (counter == 28 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D22));
					dialogueText7.color = redText.color; 
				}

				if (counter == 29 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D23));
				}

				if (counter == 30 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D24));
				}

				if (counter == 31 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D25));
					dialogueText7.color = greenText.color; 
				}

				if (counter == 32 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D26));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 33 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D27));
					dialogueText7.color = redText.color; 
				}

				if (counter == 34 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D28));
					dialogueText7.color = blueText.color; 
				}

				if (counter == 35 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D29));
				}

				if (counter == 36 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D30));
				}

				if (counter == 37 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D30P2));
				}

				if (counter == 38 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D31));
				}

				if (counter == 39 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D32));
					dialogueText7.color = redText.color; 
				}

				if (counter == 40 && cr_running == false) {
					MoveTextUp ();
					StartCoroutine(DisplayText7(D33));
					dialogueText7.color = greenText.color; 
				}

			}
			
	public void EAPortrait ()
	{
		characterPortraitEA.SetActive (true); 

		dialogueText.gameObject.SetActive (false); 
		dialogueText2.gameObject.SetActive (false);
		dialogueText3.gameObject.SetActive (false);
		dialogueText4.gameObject.SetActive (false);
		dialogueText5.gameObject.SetActive (false);
		dialogueText6.gameObject.SetActive (false);
		dialogueText7.gameObject.SetActive (false);

		Invoke ("ResetText", 2.5f);
	}

	public void ResetText ()
	{

		characterPortraitEA.SetActive (false); 

		dialogueText.gameObject.SetActive (true); 
		dialogueText2.gameObject.SetActive (true);
		dialogueText3.gameObject.SetActive (true);
		dialogueText4.gameObject.SetActive (true);
		dialogueText5.gameObject.SetActive (true);
		dialogueText6.gameObject.SetActive (true);
		dialogueText7.gameObject.SetActive (true);
	}

		
			// Displays string one character at a time
			IEnumerator DisplayText1(string text)
			{

				cr_running = true;       // Coroutine is running


				foreach(char c in text)
				{


					dialogueText.text += c;


					yield return new WaitForSecondsRealtime(characterTimeout);

				}

				cr_running = false;      // Coroutine is not running
				counter += 1;

			}


			IEnumerator DisplayText2(string text)
			{
				cr_running = true;       // Coroutine is running
				foreach(char c in text)
				{
					dialogueText2.text += c;
					yield return new WaitForSecondsRealtime(characterTimeout);
				}
				cr_running = false;      // Coroutine is not running
				counter += 1;
			}

			IEnumerator DisplayText3(string text)
			{
				cr_running = true;       // Coroutine is running
				foreach(char c in text)
				{
					dialogueText3.text += c;
					yield return new WaitForSecondsRealtime(characterTimeout);
				}
				cr_running = false;      // Coroutine is not running
				counter += 1;
			}

	IEnumerator DisplayText4(string text)
	{
		cr_running = true;       // Coroutine is running
		foreach(char c in text)
		{
			dialogueText4.text += c;
			yield return new WaitForSecondsRealtime(characterTimeout);
		}
		cr_running = false;      // Coroutine is not running
		counter += 1;
	}

	IEnumerator DisplayText5(string text)
	{
		cr_running = true;       // Coroutine is running
		foreach(char c in text)
		{
			dialogueText5.text += c;
			yield return new WaitForSecondsRealtime(characterTimeout);
		}
		cr_running = false;      // Coroutine is not running
		counter += 1;
	}

	IEnumerator DisplayText6(string text)
	{
		cr_running = true;       // Coroutine is running
		foreach(char c in text)
		{
			dialogueText6.text += c;
			yield return new WaitForSecondsRealtime(characterTimeout);
		}
		cr_running = false;      // Coroutine is not running
		counter += 1;
	}

	IEnumerator DisplayText7(string text)
	{
		cr_running = true;       // Coroutine is running
		foreach (char c in text) {
			dialogueText7.text += c;
			yield return new WaitForSecondsRealtime (characterTimeout);
		}
		cr_running = false;      // Coroutine is not running
		counter += 1;
	}

	public void MoveTextUp ()
	{
		dialogueText.text = dialogueText2.text; 
		dialogueText2.text = dialogueText3.text;
		dialogueText3.text = dialogueText4.text;
		dialogueText4.text = dialogueText5.text; 
		dialogueText5.text = dialogueText6.text;
		dialogueText6.text = dialogueText7.text;
		dialogueText7.text = ""; 

		dialogueText.color = dialogueText2.color; 
		dialogueText2.color = dialogueText3.color;
		dialogueText3.color = dialogueText4.color;
		dialogueText4.color = dialogueText5.color; 
		dialogueText5.color = dialogueText6.color;
		dialogueText6.color = dialogueText7.color;
		dialogueText7.text = ""; 
	}
}



