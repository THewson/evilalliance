﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamakazi_Script : MonoBehaviour {

    public Wave_Manager waveManager;


	// Use this for initialization
	void Start () {
        GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(waveManager.m_Target.transform.position);
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            waveManager.AddScore();
            Destroy(this.gameObject);
        }
        if(other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
        if(other.gameObject.tag == "Shield")
        {
            waveManager.AddScore();
            Destroy(this.gameObject);
        }
    }
}
