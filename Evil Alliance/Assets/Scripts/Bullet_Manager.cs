﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Manager : MonoBehaviour {

    public List<GameObject> m_GoodBulletPool;
    public List<GameObject> m_BadBulletPool;
    public List<GameObject> m_ActiveGoodBullets;
    public List<GameObject> m_ActiveBadBullets;
    
    public GameObject m_GoodBulletPrefab;
    public GameObject m_BadBulletPrefab;

    public int[] m_BulletPoolSizes = new int[2];
    public int[] m_BulletSpeed = new int[2];

	// Use this for initialization
	void Start () {
		for(int i = 0; i!= m_BulletPoolSizes[0]; i++)
        {
            m_GoodBulletPool.Add(Instantiate<GameObject>(m_GoodBulletPrefab));
            m_GoodBulletPool[i].SetActive(false);
        }

        for (int i = 0; i != m_BulletPoolSizes[1]; i++)
        {
            m_BadBulletPool.Add(Instantiate<GameObject>(m_BadBulletPrefab));
            m_BadBulletPool[i].SetActive(false);
        }
    }

    public void SpawnBullet(Vector3 position, bool isGood, Quaternion rotation)
    {
        if (isGood)
        {
            for(int i = 0; i!= m_BulletPoolSizes[0]; i++)
            {
                if (!m_GoodBulletPool[i].activeSelf)
                {
                    m_GoodBulletPool[i].transform.position = position;
                    m_GoodBulletPool[i].transform.rotation = rotation;
                    m_GoodBulletPool[i].SetActive(true);

                    m_ActiveGoodBullets.Add(m_GoodBulletPool[i]);

                    break;
                }
            }
        }
        if (!isGood)
        {
            for (int i = 0; i != m_BulletPoolSizes[1]; i++)
            {
                if (!m_BadBulletPool[i].activeSelf)
                {
                    m_BadBulletPool[i].transform.position = position;
                    m_BadBulletPool[i].transform.rotation = rotation;
                    m_BadBulletPool[i].SetActive(true);

                    m_ActiveBadBullets.Add(m_BadBulletPool[i]);
                    
                    break;
                }
            }
        }        
    }

	// Update is called once per frame
	void LateUpdate () {
		// Have bullets move forward at a certain speed
        
        if (m_ActiveGoodBullets.Count != 0)
        {
            for (int i = 0; i != m_ActiveGoodBullets.Count; i++)
            {
                m_ActiveGoodBullets[i].transform.position += m_ActiveGoodBullets[i].transform.forward * Time.deltaTime * m_BulletSpeed[0];
            }
        }

        if (m_ActiveBadBullets.Count != 0)
        {
            for(int i = 0; i!= m_ActiveBadBullets.Count; i++)
            {
                m_ActiveBadBullets[i].transform.position += m_ActiveBadBullets[i].transform.forward * Time.deltaTime * m_BulletSpeed[1];
            }
        }

        for(int i = 0; i!= m_ActiveGoodBullets.Count; i++)
        {
            if (!m_ActiveGoodBullets[i].activeSelf)
            {
                m_ActiveGoodBullets[i].transform.position = Vector3.zero;
                m_ActiveGoodBullets[i].transform.localRotation = new Quaternion(0,0,0,0);
                m_ActiveGoodBullets.Remove(m_ActiveGoodBullets[i]);
            }
        }

        for (int i = 0; i != m_ActiveBadBullets.Count; i++)
        {
            if (!m_ActiveBadBullets[i].activeSelf)
            {
                m_ActiveBadBullets[i].transform.position = Vector3.zero;
                m_ActiveBadBullets[i].transform.localRotation = new Quaternion(0, 0, 0, 0);
                m_ActiveBadBullets.Remove(m_ActiveBadBullets[i]);
            }
        }
    }
}
