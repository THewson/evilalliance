﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Change_Scene : MonoBehaviour {

	public void ChangeToScene(string sceneName) {
		SceneManager.LoadScene (sceneName);
	}

	public void ResetLevel (){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void QuitGame (){
		Application.Quit ();
	}
}
